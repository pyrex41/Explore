using DataFrames, Query, Requests, TimeSeries, ExcelReaders

function alpha_download_daily_adjusted(symb,output="TimeSeries", outp="full", key="UETDE6D6M3TS37ZG")
    base_url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol="
    url = string(base_url, symb,"&outputsize=",outp,"&apikey=",key)
    out = Requests.get(url) |> readstring |> JSON.parse
    pout = out["Time Series (Daily)"]
    k = keys(pout) |> collect |> sort
    coll =(Vector{Float64}(),Vector{Float64}(),Vector{Float64}(),Vector{Float64}(),Vector{Float64}(),Vector{Float64}(),Vector{Float64}(),Vector{Float64}())
    ind = ("1. open","2. high","3. low","4. close","5. adjusted close","6. volume","7. dividend amount","8. split coefficient")
    ind2 = ["Open","High","Low","Close","Adjusted Close","Volume","Divident Amout","Split Coefficient"]
    kk = map(x->Date(x, "yyyy-mm-dd"), k)

    for x in k
        for (i, lab) in enumerate(ind)
            push!(coll[i], pout[x][lab] |> parse |> (x->convert(Float64,x)))
        end
    end
    
    if output == "DataFrame" || output == "DataArray"
        df = DataFrame()
        df[:Date] = kk
        for (i, lab) in enumerate(ind2)
            df[Symbol(lab)] = coll[i]
        end
        out = df
    else
        out = TimeArray(kk, foldl(hcat, coll), ind2)
    end
    
    return out
end

function download_save_spreadsheet(url::String, name::String)
    gl = Requests.get(url)
    save(gl, name)
end

function get_log_ac(symb::String)
    op = alpha_download_daily_adjusted(symb, "DataFrame", "compact")
    nd = DataFrame()
    nd[:Date] = op[:Date][2:end]
    vals = map(log, op[Symbol("Adjusted Close")]) |> diff
    nd[Symbol(symb)] = vals
    return nd
end

function my_merge(a::TimeArray, b::TimeArray)
    merge(a,b,:outer)
end

function get_list_weeklies(force_new=false)
    book = ""
    url = "http://www.cboe.com/publish/weelkysmf/weeklysmf.xls"
    
    if force_new == false
        try
            book = readxlsheet("weeklysmf.xls", "Sheet1")
        catch
            download_save_spreadsheet(url, "weeklysmf.xls")
            book = readxlsheet("weeklysmf.xls", "Sheet1")
        end
    else
        download_save_spreadsheet(url, "weeklysmf.xls")
        book = readxlsheet("weeklysmf.xls", "Sheet1")
    end
    
    nb = book[12:end,:]
    df = DataFrame()
    df[:Ticker] = nb[:,1]
    df[:Type] = nb[:,4]
    df[:Weekly] = nb[:,6]
    
    wksymbs = Vector()
    
    for i=1:size(df)[1]
        if typeof(df[i,2]) != DataArrays.NAtype && typeof(df[i,3]) != DataArrays.NAtype
            if df[i,2] == "Equity" && df[i,3] == "X"
                push!(wksymbs, df[i,1])
            end
        end
    end
    return union(wksymbs)
end

function my_func(data::Array, anualized=true, verbose=false)
    y = data
    x = 1:length(y)
    
    xbar = mean(x)
    ybar = mean(y)
    
    xdif = xbar - x
    ydif = ybar - y
    
    num = sum(xdif.*ydif)
    
    xx = xdif .^2 |> sum
    yy = ydif .^2 |> sum
    
    r = num / sqrt(xx*yy)
    rsq = r^2
    slope = num / xx
    
    sl = anualized ? exp(slope)^252 - 1 : slope
    adj_slope = rsq * sl
    
    out = verbose ? (ann_slope, rsq, adj_slope) : adj_slope
    
    return out
end

function my_func(data::DataArray, anualized=false, verbose=false)
    y = data
    x = 1:length(y)
    
    xbar = mean(x)
    ybar = mean(y)
    
    xdif = xbar - x
    ydif = ybar - y
    
    num = sum(xdif.*ydif)
    
    xx = xdif .^2 |> sum
    yy = ydif .^2 |> sum
    
    r = num / sqrt(xx*yy)
    rsq = r^2
    slope = num / xx
    
    sl = anualized ? exp(slope)^252 - 1 : slope
    adj_slope = rsq * sl
    
    out = verbose ? (ann_slope, rsq, adj_slope) : adj_slope
    
    return out
end

function df_mom_regress(dff::DataFrame, window::Int)
    df_res = DataFrame()
    df_res[:Date] = dff[:Date][window:end]
    for lab in names(dff)[2:end]
        vecc = Vector()
        for i=window:size(dff)[1]
            push!(vecc,my_func(dff[lab][i-window+1:i], false))
        end
        df_res[lab] = vecc
    end
    return df_res
end

function df_mom_simple(dff::DataFrame, window::Int)
    df_res = DataFrame()
    df_res[:Date] = dff[:Date][window:end]
    for lab in names(dff)[2:end]
        vecc = Vector()
        for i=window:size(dff)[1]
            arr = dff[lab][i-window+1:i]
            push!(vecc,last(arr) - first(arr))
        end
        df_res[lab] = vecc
    end
    return df_res
end

function sort_on_row(data::DataFrame,i::Int)
    lr = data[i,:]
    nd = stack(lr, names(lr)[2:end],:Date)
    sort!(nd, cols = :value, rev = true) 
end

function select_on_row(data::DataFrame,i::Int)
    lr = data[i,:]
    nd = stack(lr, names(lr)[2:end],:Date)
    rename!(nd, :variable, :Symbol)
end

function median_table(data::DataFrame, window::Int)
    med = DataFrame()
    med[:Date] = data[:Date][window:end]
    for lab in names(data)[2:end]
        vv = []
        for i=window:size(data)[1]
            val = median(data[lab][i-window+1:i])
            push!(vv,val)
        end
        med[lab] = vv
    end
    return med
end

function diff_by_col(a::DataFrame, b::DataFrame, sorted=true,on=:Symbol, col=:value)
    nd = join(a,b, on=:Symbol, kind=:outer)
    ns = Symbol(string(col,"_1"))
    col_diff = nd[ns] - nd[col]
    nw = DataFrame()
    nw[:Date] = nd[:Date_1]
    nw[on] = nd[on]
    nw[:mom_delta] = col_diff
    sorted ? sort!(nw, cols=:mom_delta, rev=true) |> completecases! : false
    return nw
end

function merge_df_symb(a::DataFrame,b::DataFrame,on = :Symbol, kind=:outer)
    join(a,b, on = on, kind = kind)
end

function merge_df_date(a::DataFrame,b::DataFrame,on = :Date, kind=:outer)
    join(a,b, on = on, kind = kind)
end

function dual_top_decile(frame::DataFrame, full_data = false, window=21)
    ind = [size(frame)[1]]
    push!(ind, ind[1] - window) |> sort!
    
    zeroXmoms = map(i->select_on_row(frame, i), ind)
    acc = diff_by_col(zeroXmoms[1],zeroXmoms[2])
    
    cc = [zeroXmoms;;acc]
    for x in cc
        delete!(x, :Date)
    end
    
    current_data = foldl(merge_df_symb, cc)
    rename!(current_data, :value, Symbol(string("mom_lag_",window)))
    rename!(current_data, :value_1, :mom_now)
    sort!(current_data, cols=:mom_delta, rev = true) |> completecases!
    
    mom_filt = @from i in current_data begin
        @where i.mom_now > 0
        @select i
        @collect DataFrame
    end
    sort!(mom_filt, cols=:mom_delta, rev=true)
    
    mom_rank = sort_on_row(frame, size(frame)[1]) |> completecases!
    tp = (length(names(frame))-1) / 10 |> floor
    tp = convert(Int, tp)
    
    dual_top_decile = @from i in mom_filt[1:tp,:] begin
        @where in(i.Symbol, mom_rank[:variable][1:tp])
        @select i
        @collect DataFrame
    end
    
    out = full_data ? (dual_top_decile, current_data)  : dual_top_decile
end

function factor_weight(a::DataFrame, b::DataFrame, weight_a = .5, weight_b = .5, on=:Symbol)
    @assert weight_a + weight_b == 1.0
    nd = DataFrame()
    av = a[:Symbol] |> reverse
    bv = b[:Symbol] |> reverse
    
    @assert length(av) == length(bv)
    
    dic1=Dict()
    dic2=Dict()
    
    for i=1:length(av)
        push!(dic1, av[i] => i)
        push!(dic2, bv[i] => i)
    end
    
    fw = []
    wa = []
    wb = []
    
    for symb in av
        w1 = dic1[symb]
        w2 = dic2[symb]
        push!(wa,w1)
        push!(wb,w2)
        push!(fw, w1 + w2)
    end
    
    nd[:Symbol] = av
    nd[:Factor_Weight] = fw |> normalize
    nd[:Weight_A] = wa |> normalize
    nd[:Weight_B] = wb |> normalize
    sort!(nd, cols=:Factor_Weight, rev=true)
    return nd
end