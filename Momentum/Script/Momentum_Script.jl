
url = ARGS[1] |> strip
url_split = setdiff(split(url, "/"),[""])
site_url = "/"
for i in url_split
    site_url = string(site_url, i, "/")
end

println("Loading helper functions...")
include("MomentumFunctions.jl")

println("Getting list of stocks with weekly options...")
wkl = get_list_weeklies()

println("Preparing to download price data")
df_arr = []
err = []
for symb in wkl
    try
        println("Getting data for ", symb, "...")
        op = get_log_ac(symb)
        push!(df_arr, op)
    catch
        println("error for ", symb,"!!!")
        push!(err, symb)
    end
end

err2 = []
for symb in err
    try
        println("Trying again to get data for ", symb, "...")
        op = get_log_ac(symb)
        push!(df_arr, op)
    catch
        println("error again for ", symb,"!!!")
        push!(err2, symb)
    end
end

println("All done with downloads. Building log returns table")

log_df = foldl(merge_df_date, df_arr)

sort!(log_df, cols=:Date)

println("Performing 42 regression on data...")
mom_table_regress = df_mom_regress(log_df, 42)

println("Getting difference between momentum now vs 21 trading days ago...")
dual_top_regress, full_regress = dual_top_decile(mom_table_regress, true,21)

println("Computing recursive 5 day medians of log prices...")
med_lprice = median_table(log_df,5)
rec_med_lprice = median_table(med_lprice,5)

println("Building momentum table of recursively smoothed prices...")
rec_mom = df_mom_simple(rec_med_lprice,42)

dual_top_simple_rec, full_simple = dual_top_decile(rec_mom, true)

df_publish = DataFrame()
df_publish[:Symbol] = dual_top_regress[:Symbol]
for x in names(dual_top_regress)[2:end]
    df_publish[x] = map(x->round(x*100,3), dual_top_regress[x])
end
df_publish

println("Saving data...")

pur = string(site_url,"data/",Dates.today(),"/")
qur = string(site_url,"static/data/", Dates.today(),"/")
try
  mkdir(pur)
catch
end
try
  mkdir(qur)
catch
end

writetable(string(qur,"Acceleration_Regression_Full.csv"), full_regress, header=true)
writetable(string(qur,"Acceleration_Simple_Full.csv"), full_simple, header=true)

writetable(string(pur,"Dual_Top_Regress.csv"), df_publish, header=false)

nd = Dates.today()

println("Saving markdown file...")
week_day = nd |> Dates.dayname
file_text = [
    "---",
    string("title: \"", week_day, " Acceleration Rankings\""),
    string("date: ", Dates.now()),
    "draft: false",
    "---",
    "### Top Decile Acceleration w/ Positive Momentum"]

try
    str = string("{{< acc-csv url = \"data/",nd,"/Dual_Top_Regress.csv\" >}}")
    push!(file_text, str)
catch
    push!(file_text,"##### *None*")
end

push!(file_text,"<hr>")

push!(file_text,string("{{< download href=\"/data/",nd,"/Acceleration_Regression_Full.csv\" download=\"Acceleration_Regression_Full.csv\" text=\"Download Regression CSV\" >}}"))
push!(file_text, "<hr>")
push!(file_text,string("{{< download href=\"/data/",nd,"/Acceleration_Simple_Full.csv\" download=\"Acceleration_Simple_Full.csv\" text=\"Download Simple CSV\" >}}"))

f = open(string(site_url,"content/acceleration/Acceleration_",nd,".md"),"w")

for i in file_text
    write(f,i)
    write(f,"\n")
end
close(f)
