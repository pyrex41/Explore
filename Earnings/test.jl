url = ARGS[1] |> strip

url_split = setdiff(split(url, "/"), [""])

new_url = "/"

for i in url_split
    new_url = string(new_url, i,"/")
end

println(new_url)
