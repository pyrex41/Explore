url = ARGS[1] |> strip
url_split = setdiff(split(url, "/"),[""])
site_url = "/"
for i in url_split
    site_url = string(site_url, i, "/")
end


println("Loading helper functions...")
include("New_Helper_Functions.jl")

println("Getting weekly symbols from CBOE...")
url = "http://www.cboe.com/publish/weelkysmf/weeklysmf.xls"
download_save_spreadsheet(url, "weeklysmf.xls")

wk = map(x->Symbol(x),get_list_weeklies(true))

println("Getting weeklies with earnings in next 20 days from Nasdaq.com...")
#use date list to get list of underlyings with earnings with earnings in next 20 days
ndl = nasdaq_date_list()
earns = Vector()
for dt in ndl
    try
        gt = nasdaq_earnings_calendar(dt)
        for dic in gt
            try
                push!(earns, dic)
            catch
                println("error 2: ", string(dt), "; ", string(dic))
            end
        end
    catch
        println("error 1: ", string(dt))
    end
end

symbs = Vector()
for i in union(earns)
    push!(symbs, i[:_Symbol])
end
weeklySymbs = intersect(wk, symbs)

ef = list_of_dict_to_dataframe(earns)


println("Getting dispersion estimates from Nasdaq.com ...")
dispdic = Vector()
for symb in weeklySymbs
    try
        res = get_disp_nasd(symb)
        push!(dispdic, res)
    catch
        ls = ["N/A","N/A","N/A"]
        tempdic = Dict(:_Symbol => symb
            ,:consensusEstimate => ls[1]
            ,:highEstimate => ls[2]
            ,:lowEstimate => ls[3]
            )
        push!(dispdic, tempdic)
    end
end
dframe = list_of_dict_to_dataframe(dispdic)

dispframe = join(dframe, ef, on =:_Symbol, kind = :inner)


println("Getting recent close prices from Quandl...")
symbs = dispframe[:_Symbol] |> collect
prices = Vector()
priceDate = Vector()
for i in symbs
    try
        w,x = yesterday_close_quandl(i)
        push!(prices, x)
        push!(priceDate, w)
    catch
        push!(prices, "N/A")
        push!(priceDate,"N/A")
    end
end

println("Sorting and filtering...")
dispframe[:ClosePrice] = prices
dispframe[:CloseDate] = priceDate

dispframe = zeroNA(dispframe, :lowEstimate)
dispframe = zeroNA(dispframe, :highEstimate)
dispframe = zeroNA(dispframe, :consensusEstimate)

dscol = @from i in dispframe begin
    @let dispersionScore = abs((i.highEstimate - i.lowEstimate) / i.consensusEstimate)
    @select dispersionScore
    @collect
end
dispframe[:dispersionScore] = map(x->round(x.value,2),dscol)

ncd = @from i in dispframe begin
    @let delta = -(i.lowEstimate) + i.highEstimate
    @select delta
    @collect 
end
dispframe[:delta] = map(x->round(x.value,2),ncd)

tee = @from i in dispframe begin
    @where typeof(i.ClosePrice.value) == Float64 && i.ClosePrice >= 10.00 && i.dispersionScore >= .4 && i.delta >= .05
    @select i
    @collect DataFrame
end

thee = @from i in dispframe begin
    @where typeof(i.ClosePrice.value) == Float64 && i.ClosePrice >= 10.00 && i.dispersionScore >= .25 && i.delta >= .05
    @select i
    @collect DataFrame
end

tee[:EarningsDate] = map(x->Date(x,"yyyy-u-dd"),collect(tee[:EarningsDate]))
sort!(tee, cols = [order(:EarningsDate),order(:TimeOfAnnouncement), order(:_Symbol)],rev=(false,true,false))

thee[:EarningsDate] = map(x->Date(x,"yyyy-u-dd"),collect(thee[:EarningsDate]))
sort!(thee, cols = [order(:EarningsDate),order(:TimeOfAnnouncement), order(:_Symbol)],rev=(false,true,false))

println("Saving data...")
pur = string(site_url,"data/",Dates.today(),"/")
qur = string(site_url,"static/data/", Dates.today(),"/")
try
  mkdir(pur)
catch
end
try
  mkdir(qur)
catch
end

writetable(string(qur,"AllEarnings.csv"), dispframe, header=true)
size(tee)[1] > 0 ? writetable(string(pur,"High.csv"), tee, header=false) : false
size(thee)[1] > 0 ? writetable(string(pur,"Low.csv"), thee, header=false) : false

nd = Dates.today()

println("Saving markdown file...")
week_day = nd |> Dates.dayname
file_text = [
    "---",
    string("title: \"", week_day, " Earnings Strategy\""),
    string("date: ", Dates.now()),
    "draft: false",
    "---",
    "### High Threshold Candidates"]
if size(tee)[1] > 0
    str = string("{{< get-csv url = \"data/",nd,"/High.csv\" >}}")
    push!(file_text, str)
else
    push!(file_text,"##### *None*")
end
push!(file_text,"<hr>")
push!(file_text,"### Low Threshold Candidates")
if size(thee)[1] > 0
    str = string("{{< get-csv url=\"/data/",nd,"/Low.csv\" >}}")
    push!(file_text, str)
else
    push!(file_text,"##### *None*")
end
push!(file_text,"<hr>")
push!(file_text,string("{{< download href=\"/data/",nd,"/AllEarnings.csv\" download=\"All.csv\" text=\"Download All Upcoming Earnings\" >}}"))

f = open(string(site_url,"content/earnings/Earnings_",nd,".md"),"w")

for i in file_text
    write(f,i)
    write(f,"\n")
end
close(f)
