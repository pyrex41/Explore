using PyCall, Requests, DataFrames, DataArrays, DataFramesMeta, ExcelReaders

@pyimport bs4 as bs4
@pyimport requests as preq
@pyimport dateutil.parser as dparse
@pyimport xlrd as xlrd

defaultdatacols = [:Value, :Size,:Idiosync_Vol,:TS_Slope,:IV_Premium,:Low_Abs_Mom]

macro map(func, ls)
    quote
        map(x -> $func(x), $ls)
    end    
end

# function uses existing spreadsheet by default if possible, otherwise downloads new list from CBOE
function get_list_weeklies(force_new=false)
    book = ""
    
    if force_new == false
        try
            book = xlrd.open_workbook("weeklysmf.xls")
        catch
            gl = Requests.get("http://www.cboe.com/publish/weelkysmf/weeklysmf.xls")
            save(gl, "weeklysmf.xls")

            book = xlrd.open_workbook("weeklysmf.xls")
        end
    else
        gl = Requests.get("http://www.cboe.com/publish/weelkysmf/weeklysmf.xls")
        save(gl, "weeklysmf.xls")

        book = xlrd.open_workbook("weeklysmf.xls")
    end
    
    sheet = book[:sheet_by_index](0)
    
    under = Vector()
    for i in (10:sheet[:nrows]-1)
        push!(under, sheet[:row_values](i))
    end

    wksymbs = Vector()
    for i in under
        if i[4] == "Equity"
            if i[6] == "X"
                push!(wksymbs, i[1])
            end
        end
    end
    
    #univ = DataFrame()
    #univ[:_Symbol] = wksymbs
    return wksymbs
end

#gets list of non-weekend dates in format needed to goto url @ Nasdaq.com. Defaults to next 20 calendar days
function nasdaq_date_list(begindate=now(), future=20)
    ls = Vector()
    for i in begindate:(begindate + Dates.Day(future))
        m = Dates.monthname(i)[1:3]
        wkd = Dates.dayofweek(i)
        d = Dates.day(i)
        if d < 10
            d = string("0",d)
        end
        y = Dates.year(i)
        
        str = string(y,"-",m,"-",d)
        
        if wkd <= 5
            push!(ls, str)
        end
    end
    
    return ls
end

#Looks up earnings calendar for specific date @ Nasdaq.com
function nasdaq_earnings_calendar(date)
    hoppy = Vector()
    
    url = string("http://www.nasdaq.com/earnings/earnings-calendar.aspx?date=", date)
    html = preq.get(url)
    coldsoup = html[:text]
    soup = pycall(bs4.BeautifulSoup, PyAny, coldsoup, "lxml")
    bowl = soup[:find_all]("tr")
    
    for cereal in bowl
        if length(cereal[:contents]) > 17
            
            
            s = cereal[:contents][4][:a][:attrs]
            if contains(s["href"], "http://www.nasdaq.com/earnings/report")
                
                mnorev = ""
                try
                    anb = cereal[:contents][2][:a][:attrs]

                    if contains(anb["href"],"after-hours")
                        mnorev = "after-hours"
                    elseif contains(anb["href"],"premarket")
                        mnorev = "premarket"
                    else
                        mnorev = "NA"
                    end
                catch
                    mnorev = "NA"
                end

                
                
                pister = cereal[:contents][4][:text]
                reg = "\\(([^a-z\)]+)\\)"
                sym = match(Regex(reg), pister)[1]

                krug = cereal[:contents][6][:text]
                cate = dparse.parse(krug, fuzzy = true) |> Date
                
                if mnorev == "premarket"
                    strdate = cate - Dates.Day(1)
                    ltrdate = cate - Dates.Day(10)
                elseif mnorev == "after-hours"
                    strdate = cate
                    ltrdate = cate - Dates.Day(9)                    
                else
                    strdate = "NA"
                    ltrdate = "NA"
                end

                dictemp = Dict(:EarningsDate => cate, :_Symbol => sym, :TimeOfAnnouncement => mnorev, :ShortTradeDate => strdate, :LongTradeDate => ltrdate)

                push!(hoppy, dictemp)
            end
        end
    end
    return hoppy
end

# take list of dictionaries and convert to dataframe
function list_of_dict_to_dataframe(diclist)
    kays = collect(keys(diclist[1]))

    outframe = DataFrame()

    for key in kays
        vals = Vector()
        for dic in diclist
            push!(vals, dic[key])
        end
        outframe[key] = vals
    end
    return outframe
end

#get dispersion estimate from yahoo
function get_dispersion_est(symb)
    url = string("http://finance.yahoo.com/q/ae?s=",symb,"+Analyst+Estimates.html")
    headers = Dict("user-agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
    hhhh= preq.get(url, headers=headers)
    coldsoup = hhhh[:text]
    soup = pycall(bs4.BeautifulSoup, PyAny, coldsoup, "html.parser")
    bowl = soup[:find_all]("tr")

    ind_c = -1
    ind_l = -1
    ind_h = -1
    ind_s = -1

    for i in 1:length(bowl)
        try
            if bowl[i][:contents][1][:text] == "Earnings Est"
                ind_c = i + 1
                ind_l = i + 3
                ind_h = i + 4
            end
        catch
        end

        try
            if bowl[i][:contents][1][:text] == "Earnings History"
                ind_s = i + 4
            end
        catch
        end

    end
    
    indi = [ind_c, ind_l, ind_h]
    
    est = Vector()
    
    for i in indi
        try
            push!(est, parse(Float64, bowl[i][:contents][2][:text]))
        catch
            push!(est, "N/A")
        end
    end

    try
        push!(est, round(parse(Float64, bowl[ind_s][:contents][2][:text][1:(end-1)]) * .01, 2))
    catch
        push!(est, "N/A")
    end
    

    tempdic = Dict(:_Symbol => symb
    ,:consensusEstimate => est[1]
    ,:lowEstimate => est[2]
    ,:highEstimate => est[3])

    tempdic
end

# get dispersion estimate from nasdaq.com
function get_disp_nasd(symb)
    url = string("http://www.nasdaq.com/symbol/",symb,"/earnings-forecast")
    headers = Dict("user-agent" => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
    hhhh= preq.get(url, headers=headers)
    coldsoup = hhhh[:text]
    soup = pycall(bs4.BeautifulSoup, PyAny, coldsoup, "html.parser")
    bowl = soup[:find_all]("tr")
    
    test = bowl[14]("td")
    
    est = Vector()
    for i=2:4
        try
            push!(est, parse(Float64,test[i][:text]))
        catch
            push!(est, "N/A")
        end
    end
    
    tempdic = Dict(:_Symbol => symb
    ,:consensusEstimate => est[1]
    ,:highEstimate => est[2]
    ,:lowEstimate => est[3])

    return tempdic
end

# quickly replace N/A in a df column with zeros
macro zeroNA(df, col)
    quote
        @byrow! $df if $col == "N/A"; $col = 0 end
    end
end 