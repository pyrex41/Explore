args = [:yahoo,"/Users/reuben/EarningsScrapeData/", "/Users/reuben/Google\ Drive/RevisedTradeScrape/"]

if length(ARGS) > 0
    for i = 1:length(ARGS)
        args[i] = ARGS[i]
    end
end

earnings_source = args[1]
data_folder = args[2]
gdrive = args[3]

include("ScrapeHelperFunctions.jl")

tod = Date(now()) |> string

#make folder for today's data
gdir = pwd()
nm = string(tod, "_scrape_data")
cd(data_folder)
mkdir(nm)

#get and clean list of stocks with weekly options from CBOE
wk = get_list_weeklies(true)
t = union(wk)

#get list of trade dates from next 20 days to get data from nasdaq
ndl = nasdaq_date_list()

#use date list to get list of underlyings with earnings with earnings in next 20 days
earns = Vector()
for dt in ndl
    gt = nasdaq_earnings_calendar(dt)
    for dic in gt
        push!(earns, dic)
    end
end

println("Nasdaq earnings calendar scrape successful")


# set function to use yahoo or nasdaq data
if earnings_source == :yahoo
    scfunc = :get_dispersion_est
else
    scfunc = :get_disp_nasd
end

#get symbs from next 20 days
symbs = Vector()
for i in union(earns)
    push!(symbs, i[:_Symbol])
end

#get only weekly symbols
weeklySymbs = intersect(wk, symbs)

#put earnings dates in dataframe
ef = list_of_dict_to_dataframe(earns)

# get the dispersion data from nasdaq (default) or yahoo
dispdic = Vector()
for symb in weeklySymbs
    try
        res = eval(scfunc)(symb)
        push!(dispdic, res)
    catch
        ls = ["N/A","N/A","N/A"]
        tempdic = Dict(:_Symbol => symb
            ,:consensusEstimate => ls[1]
            ,:highEstimate => ls[2]
            ,:lowEstimate => ls[3])        
        push!(dispdic, tempdic)
    end
end
dframe = list_of_dict_to_dataframe(dispdic)

dispframe = join(dframe, ef, on = :_Symbol, kind = :inner)

println("Dispersion Scrape successful")

# get dispersion score
dscore = Vector()
for i in 1:length(dispframe[:1])
    ds = ""
    try
        ds = abs((dispframe[:highEstimate][i] - dispframe[:lowEstimate][i]) / dispframe[:consensusEstimate][i])
        ds = round(ds, 2)
    catch
        ds = 0
    end
    push!(dscore, ds)
end

#add dispersion score to earnings dataaframe
dispframe[:dispersionScore] = dscore
println("Dispersion calculations added")

#filter out dispersion < .4, absolute difference < .05
@zeroNA dispframe :lowEstimate
@zeroNA dispframe :highEstimate
nf = @transform(dispframe, delta = round(- :lowEstimate + :highEstimate, 2))
filt = @where(nf, :dispersionScore .>= .4)
thee = @where(filt, abs(:delta) .>= .05)

#calculate helpful trading dates and add to dataframe
c = Vector()
for i in 1:length(thee[1])
    
    val = "" 
    
    try
        val = thee[:ShortTradeDate][i] - Date(now())
    catch
        val = NA
    end
    
    push!(c, val)
end
thee[:daysUntilEarnings] = c
thee = sort!(thee, cols = :daysUntilEarnings)

println("Helpful dates added")

#write dataframe to file
cd(nm)
writetable(string("next20days_",tod,"_",earnings_source,".csv"),thee)
cd(gdir)
println("Data saved to .csv")

#Save data to google drive
cp(string(data_folder, nm), string(gdrive,"Output/", nm), remove_destination = true)
println("Next 20 days copied to output file")
