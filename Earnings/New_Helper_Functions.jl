mods = ["ExcelReaders","Requests", "DataFrames", "Gumbo", "AbstractTrees", "Query"]
inst = keys(Pkg.installed()) |> collect

for i in setdiff(mods, inst)
   Pkg.add(i)
end

using ExcelReaders, Requests, DataFrames, Gumbo, AbstractTrees, Query

function download_save_spreadsheet(url::String, name::String)
    gl = Requests.get(url)
    save(gl, name)
end

function get_list_weeklies(force_new=false)
    book = ""
    url = "http://www.cboe.com/publish/weelkysmf/weeklysmf.xls"
    
    if force_new == false
        try
            book = readxlsheet("weeklysmf.xls", "Sheet1")
        catch
            download_save_spreadsheet(url, "weeklysmf.xls")
            book = readxlsheet("weeklysmf.xls", "Sheet1")
        end
    else
        download_save_spreadsheet(url, "weeklysmf.xls")
        book = readxlsheet("weeklysmf.xls", "Sheet1")
    end
    
    nb = book[12:end,:]
    df = DataFrame()
    df[:Ticker] = nb[:,1]
    df[:Type] = nb[:,4]
    df[:Weekly] = nb[:,6]
    
    wksymbs = Vector()
    
    for i=1:size(df)[1]
        if typeof(df[i,2]) != DataArrays.NAtype && typeof(df[i,3]) != DataArrays.NAtype
            if df[i,2] == "Equity" && df[i,3] == "X"
                push!(wksymbs, df[i,1])
            end
        end
    end
    return union(wksymbs)
end

function nasdaq_date_list(begindate=now(), future=20)
    ls = Vector()
    for i in begindate:(begindate + Dates.Day(future))
        m = Dates.monthname(i)[1:3]
        wkd = Dates.dayofweek(i)
        d = Dates.day(i)
        d < 10 ? d = string("0", d) : false
        y = Dates.year(i)
        str = string(y,"-",m,"-",d)
        wkd <= 5 ? push!(ls, str) : false
    end
    return ls
end

function parse_table(tab)
    lines = tab.children[2].children

    ticker = Vector()
    day = Vector()
    for i in lines
        try
            xi = i.children[1].children[1]
            xj = split(getattr(xi, "href"), "/")
            sym = xj[end-1] |> uppercase |> Symbol
            tm = xj[end] |> Symbol
            push!(ticker, sym)
            push!(day, tm)
        catch
            println("error")
        end
    end
    return ticker, day
end

function nasdaq_earnings_calendar(date::String)
    hoppy = Vector()

    url = string("http://www.nasdaq.com/earnings/earnings-calendar.aspx?date=",date)
    html = Requests.get(url).data |> copy |> String |> parsehtml
    tr_coll = Vector()
    coll = PreOrderDFS(html.root)
    tables = Vector()
    for i in coll
        if typeof(i) == Gumbo.HTMLElement{:table}
            try
                cl = getattr(i, "class")
                if cl == "USMN_EarningsCalendar"
                    push!(tables,i)
                end
            catch
            end
        end
    end
    t, d = parse_table(tables[1])

    d = map(d) do x
        x == Symbol("after-hours") ? :after_hours : x
    end

    addit = map(d) do x
        x == :premarket ? -1 : 0
    end
    shortdate = map(addit) do x
        Dates.today() + Dates.Day(x)
    end

    longdate = map(x->x - Dates.Day(10),shortdate)

    for i=1:length(t)
        dictemp = Dict(
            :EarningsDate => date,
            :_Symbol => t[i],
            :TimeOfAnnouncement => d[i],
            :ShortTrade => shortdate[i],
            :LongTradeDate => longdate[i])
        push!(hoppy, dictemp)
    end
    return hoppy
end

function list_of_dict_to_dataframe(diclist)
    kays = collect(keys(diclist[1]))

    outframe = DataFrame()

    for key in kays
        vals = Vector()
        for dic in diclist
            push!(vals, dic[key])
        end
        outframe[key] = vals
    end
    return outframe
end

function get_disp_nasd(symb)
    url = string("http://www.nasdaq.com/symbol/",symb,"/earnings-forecast")
    html = Requests.get(url).data |> copy |> String |> parsehtml
    coll = PreOrderDFS(html.root)
    cc= Vector()
    for i in coll
        if typeof(i) == Gumbo.HTMLElement{:table}
            tt = ""
            try
                tt = i.children[1].children[1].children[1].children[3].text
                if contains(tt, "Quarter")
                    bb = i.children[2].children[1].children
                    cc = map(x -> x.children[1],bb)[2:5]
                    cc = map(x->x.text, cc)
                    cc = map(parse,cc)
                end
            catch
                tt = "no"
            end
        end
    end
    tempdic = Dict(
        :_Symbol => symb,
        :consensusEstimate => cc[1],
        :highEstimate => cc[2],
        :lowEstimate => cc[3]
    )
    return tempdic
end

function zeroNA(df::DataFrame, col::Symbol)
    oc = df[col] |> collect
    nc = map(oc) do x
        x == "N/A" ? 0 : x
    end
    df[col] = nc
    return df
end

function yesterday_close_quandl(symb, api="VhLmhL-NbYjbUmBSyDA6")
    url = string("https://www.quandl.com/api/v3/datatables/WIKI/PRICES.json?ticker=",symb,"&api_key=",api)
    req = Requests.get(url)
    dt = (req.data |> String |> JSON.parse)["datatable"]
    temp = last(dt["data"])
    dt, pr = temp[2],temp[3]
end
