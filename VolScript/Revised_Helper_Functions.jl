#Revised Helper Functions
using Plots, Quandl,DataFrames, TimeSeries, Gumbo, AbstractTrees, JSON, Requests, ReactiveBasics,  IndexedTables

# Reuben's authentication token for Quandl
auth_default = "VhLmhL-NbYjbUmBSyDA6"

# building codes for futures data
base_codes = ["F","G","H","J","K","M","N","Q","U","V","X","Z"]
codes = foldl(vcat,map(y->map(x->string("CBOE/VX",x,y),base_codes),["2015","2016","2017", "2018"]))

# get free futures data from quandl, in an array of TimeSeries
function futures_data(auth = auth_default, cod = codes)
    out = []
    c_out = []
    for x in cod
        try
            req = quandlget(x,api_key = auth)
	    if typeof(req) != Core.TypeofBottom
              push!(out, req)
              push!(c_out, x)
	    else
	      println("blank data for ", x)
	    end
        catch
            println("There is no data for ", x)
        end
    end
    return c_out, out
end

# scrape CBOE delayed quote page
function get_CBOE_data()
    url1 = "http://www.cboe.com/delayedquote/futures-quotes"
    bowl = Requests.get(url1)
    return html = parsehtml(String(copy(bowl.data)))
end

# take CBOE delayed quote page and returns 4 arrays: quandl labels, dates,
# values, and days until settlement
function get_future_settlements(html)
    leaves = Leaves(html.root) |> collect
    ll = leaves |> length

    kv,dv,pv,rv = [],[],[],[]

    for i=1:ll
        if typeof(leaves[i]) == Gumbo.HTMLText
            if contains(leaves[i].text, "VIX/")
                k = (leaves[i].text |> strip)[(end-1):end]
                push!(kv, string("CBOE/VX",k[1],"201",k[2]))
                d = leaves[i+1].text |> strip
                dd = Date(d, "mm/dd/yyyy")
                push!(dv, dd)
                push!(pv, leaves[i+2].text |> strip |> parse)
                push!(rv, dd - Dates.today() |> Dates.value)
            end
        end
    end
    return kv, dv, pv, rv
end

# take settlement data and futures data and get array of settlement dates in date format
function get_settlement_dates(sdata, fdata)
    settle_dates = sdata[2]
    s1 = sort(map(x->last(x.timestamp),fdata))
    setf = vcat(s1,settle_dates) |> union
    setf = length(setf) >= length(s1) ? setdiff(setf, [last(s1)]) : setf
    sf = Date[]
    for i in setf
        push!(sf,i)
    end
    return sf
end

# merge futures data into one TimeArray
function futures_merge(fut_hist, codes)
    llama = map(x->x["Close"],fut_hist)
    emu = foldl(llama) do a,b
        merge(a,b,:outer)
    end
    @assert length(codes) >= length(emu.colnames)
    for i=1:length(emu.colnames)
        emu.colnames[i] = codes[i]
    end
    return emu
end

# Helper function for get_cm() to get front and back month relative to synthetic future date,
# along with weighting of front month for interpolation
# Returns input date, front month code, back month code, and fronth month
# weighting in a vector.
function get_months(val, codes, settle, cm)
    dtes = map(x->x.value,(settle - val))
    f,b,wf = 0,0,0

    for i=2:length(dtes)
        if dtes[i - 1] > 0 && dtes[i-1] <= cm && dtes[i] > cm
            b = i
            f = i -1
            wf = dtes[i-1] / cm
        end
    end

    out, out2 = map(x-> (x != 0 ? codes[x] : "NA"),[f,b])
    out3 = round(wf,3)
    return [val,out,out2,out3]
end

# Calculate synthetic future price at synth number of days via interpolation
function get_cm(h, t, codes, sf, synth::Int)
    m = get_months(t, codes, sf, synth)
    row = h[t]
    f = row[m[2]].values[1]
    b = row[m[3]].values[1]
    wf = m[4]
    cmt = wf * f + (1 -wf) * b
    return cmt
end

# Returns the daily roll of the front month VIX future versus the next month
# Uses 2nd and 3rd future if 1st month < 5 days
function get_front_roll_months(day::Date, h, sf, codes, dt = 5)
    o2 = 0.0
    try
        dges = map(x->x.value,(sf - day))
        f,b= 0,0

        for i in 2:(length(dges)-1)
            if dges[i - 1] < dt && dges[i] >= dt
                    b = i + 1
                    f = i
            end
        end
        out = map(x-> h[x][day].values[1],map(x-> codes[x], [f,b]))
        o2 = round(out[1]/out[2],3)
    catch
        o2 = NaN
    end
    return o2
end

# Signal macro that returns a signal with the median of the last X values
# X defaults to 5
macro medWin(sig,win = 5)
    quote
        arr = []
        flatmap($(esc(sig))) do iv
            unshift!(arr, iv)
            length(arr) > $(esc(win)) ? pop!(arr) : false
            median(arr) |> Signal
        end
    end
end

# macro takes signal and returns a signal that always has an up-to-date array
# of all past values of the original signal
macro collect(sig)
    quote
        arr = []
        out = flatmap($sig) do s
            push!(arr, s) |> Signal
        end
    end
end

# function to map the @collect macro onto an array of signals
function mapcollect(arr)
    out = []
    for i =(arr)
        t = @eval (@collect $i)
        push!(out,t)
    end
    out
end

# function to calculate daily roll [(vix - front vx future) / days till #irst
# future settles]
function daily_roll(day, vix_value, h, sf, codes, dt = 10)
    dges = [x.value for x in (sf-day)]
    ff = 0
    try
        if dges[1] >= dt
            ff = 1
        else
            for i in 2:length(dges)
                if dges[i-1] < dt && dges[i] >= dt
                    ff = i
                end
            end
        end
    catch
        ff = NaN
    end
    vx = h[codes[ff]][day].values[1]
    return round((vix_value - vx) / dges[ff], 3)
end

function my_transpose(vec::Vector)
    reshape(vec, 1, length(vec))
end

function quick_plot(df::DataFrame, x::Symbol, ind::Vector)
    Plots.plot(df[x], map(x->df[x], ind), label=my_transpose(map(string, ind)))
end

#loads google page with future prices
function load_google_vix_fut_data()
    mon = Dates.monthname(Dates.today() + Dates.Month(1))
    mstr = mon[1:3]
    url = string("https://finance.google.com/finance?q=INDEXCBOE%3AVIX", mstr)
    bowl = Requests.get(url)
    html = parsehtml(String(copy(bowl.data)))
end

# scrapes google page for last future trading date
function get_recent_future_date(html)
    yc = []

    gumbr = PreOrderDFS(html.root)
    
    dt = Dates.today()
    tdt=""
    yyc=[]
    
    for i in gumbr
        if typeof(i) == Gumbo.HTMLElement{:span}
            try
                at = getattr(i, "class")
                if at == "nwp"
                    tdt = strip(strip(i.children[1].text)[1:6])
                end
            catch
            end
        end
    end
   
    if (Dates.today() |> Dates.monthday) == (1,1)
        yy = ((Dates.today() |> Dates.year) - Dates.year(1)) |> string
    else
        yy = Dates.today() |> Dates.year |> string
    end

    if tdt == "Delaye"
        the_date = dt
    else
        try
            the_date = Date(string(tdt," ", yy),"uuu dd yyyy")
        catch
            the_date = "error"
        end
    end
    
    return the_date
end
