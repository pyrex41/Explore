
println("Loading helper functions...")

include("Revised_Helper_Functions.jl")

println("Starting...")
#set working directory in script
if length(ARGS) > 0
    str = ARGS[1]
    if str[end] == "/"
      wd  = str[1:end-1]
    else
      wd = str
    end
else
    wd = pwd()
end

# get futures data from quandl
println("Getting futures data")
c_out, fdata = futures_data()

futDataInit = map(x->x["Close"], fdata)

println("Getting CBOE data")
cboe_data = get_CBOE_data()
setData = get_future_settlements(cboe_data)

println("Get recent future data from Google.")
rec_date = load_google_vix_fut_data() |> get_recent_future_date

n_dict = Dict()
for i in 1:length(setData[1])
    push!(n_dict,setData[1][i] => setData[3][i])
end
n_dict

ts_dict = Dict()
for i=1:length(c_out)
    push!(ts_dict, c_out[i] => futDataInit[i])
end

futData = []

for i in c_out
    if haskey(n_dict, i)
         try
              nn = update(ts_dict[i], rec_date, n_dict[i])
              push!(futData, nn)
          catch
              println("Problem adding google data; is it a weekend?")
              push!(futData, ts_dict[i]) 
        end

    else
        push!(futData, ts_dict[i])
    end
end

# get settlement dates
settle_dates = get_settlement_dates(setData, futData)

println("Getting quandl data.")
# get vix, vxv data from quandl
vixf,vxvf = map(x->quandlget(x,from = "01/01/2015",api_key=auth_default), ["CBOE/VIX","CBOE/VXV"])
vix = vixf["VIX Close"]
vxv = vxvf["CLOSE"]

# merge all futures data into one TimeArray
futures_close_timeseries = futures_merge(futData, codes)

# Get list of dates with data from both vix and futures
ttn = intersect(futures_close_timeseries.timestamp, vix.timestamp)

# Initiate signal with initial date with data
f = Signal(ttn[1])

println("Building backtest system...")
# Initiate signal that calculates synthetic 45 day continuous future price
cm45 = flatmap(f) do dt
    round(get_cm(futures_close_timeseries, dt, codes, settle_dates, 45),3) |> Signal
end

# Initiate signal with value of Vix on date in signal f
vix_sig = flatmap(f) do dt
    (dt >= vix.timestamp[1] ? vix[dt].values[1] : 0.0) |> Signal
end

# Initiate signal with value of vxv on date in signal f
vxv_sig = flatmap(f) do dt
    (dt >= vxv.timestamp[1] ? vxv[dt].values[1] : 0.0) |> Signal
end

# Initiate signal that calulates IVTS (VIX / VXV)
ivts_sig = flatmap(vxv_sig) do vxv
    vix = vix_sig.value
    round(vix / vxv, 3) |> Signal
end

# initiate signal that calculates IVTS with 45 day continuous futures
# (VIX / CM45)
roll_vs_45 = flatmap(vix_sig) do v
    c = cm45.value
    round(v / c, 3) |> Signal
end

# calculates roll of 1st month VIX future vs 2nd month
# (uses 2nd and 3rd months if 1st month < 5 days to settlement)
front_roll = flatmap(f) do dt
    get_front_roll_months(dt, futures_close_timeseries, settle_dates, codes, 10) |> Signal
end

# daily roll signal from David Simon paper
daily_roll_sig = flatmap(vix_sig) do vix
    dd = f.value
    daily_roll(dd, vix, futures_close_timeseries, settle_dates, codes, 10) |> Signal
end

# Vector of signals
rawSigs = [ivts_sig, roll_vs_45, front_roll, daily_roll_sig]

# Vector with a median 5day filter mapped on the raw signals
medSigs = map(x->(@medWin x), rawSigs)

# Vector with a second 5day filter mapped onto the filtered results
# (retursn array of recursively median filtered signals)
recSigs = map(x->(@medWin x), medSigs)

# Array with recursively filtered signals rounded to 3 decimal places
smoothSigs = map(recSigs) do s
    flatmap(s) do sig
        round(sig,3) |> Signal
    end
end

# Array of signals that each hold the collected values of each
# of the raw signals
rawVecs = rawSigs |> mapcollect

# Array of signals that each hold the collected values of each
# of the median filtered signals
medVecs = medSigs |> mapcollect

# Array of signals that each hold the collected values of each
# of the recursively median filtered signals
recVecs = smoothSigs |> mapcollect

# Array that collects all the dates that have been pushed to signal f
x = @collect f

println("Running backtest...")
# Run backtest by pushing dates in ttn to signal f
# uncomment println's for troubleshooting / verbose mode
for i = 2:length(ttn)
    push!(f, ttn[i])
    #println(value(f))
    #map( x->(x |> value |> println), medSigs)
    #map( x->(x |> value |> println), smoothSigs)
    #println("---------")
end

# get labels for dataframe
sig1_label = ["VIX / VXV", "VIX / CM45", "Front Month Roll", "Daily Roll"]
smooth_label = map( x->string(x, " (smoothed)"), sig1_label)
labels = [sig1_label;smooth_label]

#Build dataframe with data from backtest
dff = DataFrame()
dff[:Date] = x.value |> DataArray
rawd =foldl(hcat,map( x->(x.value |> DataArray), rawVecs))
recd = foldl(hcat,map( x->(x.value |> DataArray), recVecs))
for i=1:8
    dff[Symbol(labels[i])] = hcat(rawd,recd)[:,i]
end

#get rid of initialization window
dfn = dff[16:end,:]

# Recent Signals only
dflim = dfn[end-20:end,:]

# Current signals only
ll = dflim[end,:]

# relevant signals
Daily_Roll = ll[5][1]
IVTS_CM45_filt = ll[7][1]

# function to parse signals
function sig_filt(val, short, long)
    if val <= short
        out = "short"
    elseif val >= long
        out = "long"
    else
        out = "flat"
    end
    return out
end

#current signals in text
iv_sig = sig_filt(IVTS_CM45_filt, .91, 1.10)
dr_sig = sig_filt(Daily_Roll, -.1, .1)

txmsg=[
    string("Today's smooth CM45 IVTS signal is ", IVTS_CM45_filt, ". Strategy signal: *", iv_sig,"*."),
    string("Today's Daily Roll is ", Daily_Roll,". Strategy signal: *", dr_sig,"*.")
]

println("Saving csv data...")
#use only for script
# change to data directory
cd(string(wd, "/static/data/"))


nd = string(Dates.today())
try
  cd(nd)
catch
  mkdir(nd)
  cd(nd)
end

writetable("Recent_Data.csv", dflim)
writetable("Full_Data.csv", dfn)

println("Plotting and saving plots.")
# change to plotting directory
cd(string(wd,"/static/images/"))
try
  cd(nd)
catch
  mkdir(nd)
  cd(nd)
end

# col indexes for plotting
ind1 = [names(dfn)[2],names(dfn)[6]]
ind2 = [names(dfn)[3],names(dfn)[7]]
ind3 = [names(dfn)[4],names(dfn)[8]]
ind4 = [names(dfn)[5],names(dfn)[9]]

# PLOTTING
quick_plot(dfn, :Date, ind1)
# Save plot of raw signals
savefig("ivts_full.png")

quick_plot(dfn, :Date, ind2)
# Save plot of recursively filtered signals
savefig("cm45_full.png")

quick_plot(dfn, :Date, ind3)
savefig("frontmonth_roll_full.png")

quick_plot(dfn, :Date, ind4)
savefig("daily_roll_full.png")

quick_plot(dflim, :Date, ind1)
savefig("ivts_recent.png")

# Save plot of raw signals
quick_plot(dflim, :Date, ind2)
savefig("cm45_recent.png")

# Save plot of raw signals
quick_plot(dflim, :Date, ind3)
savefig("frontmonth_roll_recent.png")

# Save plot of raw signals
quick_plot(dflim, :Date, ind4)
savefig("daily_roll_recent.png")

println("Saving content file...")
# switch to content directory
cd(string(wd,"/content/vol/"))

# write content file in markdown
week_day = Dates.today() |> Dates.dayname

file_text = [
    "---",
    string("title: \"", week_day, " Vol Strategy\""),
    string("date: ", Dates.now()),
    "draft: false",
    "---",
    "### Mojito 3.0 Indicator",
    txmsg[1],
    "<br>",
    "#### Recent CM45",
    string("{{< myimg src=\"/images/",nd,"/cm45_recent.png\" >}}"),
    "<br>",
    "<hr>",
    "<br>",
    "### Daily Roll Indicator",
    txmsg[2],
    "<br>",
    "#### Recent Daily Roll",
    string("{{< myimg src=\"/images/",nd,"/daily_roll_recent.png\" >}}"),
    "<hr>",
    string("{{< download href=\"/data/",nd,"/Recent_Data.csv\" download=\"Recent.csv\" text=\"Download Recent Data\" >}}"),
    "<br>",
    string("{{< download href=\"/data/",nd,"/Full_Data.csv\" download=\"Full.csv\" text=\"Download Full Data\" >}}")
]

f = open(string(nd,".md"),"w")

for i in file_text
    write(f,i)
    write(f,"\n")
end
close(f)
